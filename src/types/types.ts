import type { ApplicationProfile } from "@/types/application-profile";
import type { RemovableRef } from "@vueuse/core";
import type { OrchestratedToast } from "bootstrap-vue-next";

export type NotificationTranslationProps = {
  keypath: string;
  tag?: string;
  placeholders: Record<string, { el: string; class: string; value: string }>;
};

export type NotificationToast = OrchestratedToast & {
  variant?: "danger" | "warning" | "success" | "info" | "primary" | "secondary";
  component?: NotificationTranslationProps;
  autoHide?: boolean;
};

export type Theme = "light" | "dark";

export interface MainState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  aims: {
    drafts: RemovableRef<Record<string, ApplicationProfile>>;
  };
  coscine: {
    theme: RemovableRef<Theme>;
  };
}
export interface NotificationState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  notificationQueue: NotificationToast[];
}
export interface CoscineErrorResponse {
  data: {
    status: number;
    instance: string;
  };
  traceId: string;
  statusCode: number;
  isSuccess: boolean;
}

import type State from "./state";

export interface ApplicationProfileInfo {
  accessToken?: string;
  approvedBy?: string[];
  created?: string;
  creator?: string;
  definition?: string;
  description?: string;
  inheritedFrom?: string[];
  mimeType?: string;
  state?: State;
  underReviewBy?: string[];
  versions?: string[];
}

export interface ApplicationProfile extends ApplicationProfileInfo {
  base_url: string;
  name: string;
}

export interface ApplicationProfileSearchParameters {
  community?: string | null;
  creator?: string;
  from?: string;
  language: "de" | "en";
  state?: "public" | "approved";
  subject?: string | null;
  to?: string;
}

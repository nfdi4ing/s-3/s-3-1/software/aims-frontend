import { defineStore } from "pinia";
import type { AxiosError } from "axios";
import { getReasonPhrase } from "http-status-codes";
import { i18n } from "@/plugins/vue-i18n";
import {
  CoscineErrorResponse,
  NotificationState,
  NotificationToast,
} from "../types/types";

/*
  Store variable name is "this.<id>Store"
    id: "notification" --> this.notificationStore
  Important! The id must be unique for every store.
*/
export const useNotificationStore = defineStore({
  id: "notification",

  /*
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): NotificationState => ({
    notificationQueue: [] as NotificationToast[],
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.notificationStore.<getter_name>;
  */
  getters: {},

  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.notificationStore.<action_name>();
  */
  actions: {
    // Handles all API error notifications
    postApiErrorNotification(error: AxiosError) {
      let notification: NotificationToast;

      if (error.response) {
        const status = error.response.status;
        const coscineErrorResponse = error.response
          .data as CoscineErrorResponse;

        // eslint-disable-next-line unicorn/prefer-ternary
        if (status === 401) {
          notification = {
            title: i18n.global.t("apiError.unauthorized.title").toString(),
            body: i18n.global.t("apiError.unauthorized.body").toString(),
            variant: "warning",
            autoHide: false,
          };
        } else {
          // The request was made and the server responded with a status code that falls out of the range of 2xx
          notification = {
            title: i18n.global.t("apiError.specific.title").toString(),
            component: {
              keypath: "apiError.specific.body",
              tag: "span",
              placeholders: {
                error: {
                  el: "span",
                  class: "",
                  value: `(${status}: ${getReasonPhrase(status)})`,
                },
                traceId: {
                  el: "span",
                  class: "text-monospace",
                  value: coscineErrorResponse.traceId ?? status,
                },
              },
            },
            variant: status >= 500 ? "danger" : "warning",
            autoHide: false,
          };
        }
      } else {
        // Something happened in setting up the request that triggered an Error
        notification = {
          title: i18n.global.t("apiError.general.title").toString(),
          body: i18n.global.t("apiError.general.body").toString(),
          variant: "danger",
        };
      }

      // Post the notification to the store
      this.postNotification(notification);
    },

    // Can be used for general warnings
    postGeneralApiWarningNotification(body: string) {
      // Something happened in setting up the request that triggered an Error
      const notification = {
        title: i18n.global.t("toast.apiError.general.title").toString(),
        body: i18n.global
          .t("toast.apiError.specific.body", { error: body })
          .toString(),
        variant: "warning",
      } as NotificationToast;
      this.postNotification(notification);
    },

    // TODO: Make use of the success notification system
    postSuccessNotification(body: string) {
      const notification: NotificationToast = {
        variant: "success",
        autoHide: true,
      };
      this.postNotification(notification);
    },
    
    // Pushes the notification in the queue 
    postNotification(toast: NotificationToast) {
      this.notificationQueue.push(toast); // TODO: Address this warning
    },

    // Deletes the notification from the queue
    deleteNotification(toast: NotificationToast) {
      const index = this.notificationQueue.indexOf(toast);
      this.notificationQueue.splice(index);
    },
  },
});

export default useNotificationStore;

import type {
  ApplicationProfile,
  ApplicationProfileInfo,
} from "@/types/application-profile";
import type State from "@/types/state";

class ApplicationProfileImpl implements ApplicationProfile {
  name: string;
  base_url: string;
  accessToken?: string;
  created?: string;
  creator?: string;
  definition?: string;
  description?: string;
  mimeType?: string = "application/ld+json";
  state?: State;

  constructor(
    name: string,
    base_url: string,
    applicationProfileInfo: ApplicationProfileInfo = {},
  ) {
    this.name = name;
    this.base_url = base_url;

    this.accessToken = applicationProfileInfo.accessToken;
    this.created = applicationProfileInfo.created;
    this.creator = applicationProfileInfo.creator;
    this.definition = applicationProfileInfo.definition;
    this.description = applicationProfileInfo.description;
    this.mimeType = applicationProfileInfo.mimeType ?? "application/ld+json";
    this.state = applicationProfileInfo.state;
  }
}

export default ApplicationProfileImpl;

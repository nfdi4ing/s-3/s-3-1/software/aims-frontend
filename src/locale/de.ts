export default {
  nav: {
    brand: "Metadata Profile Service",
    extrasHelp: "Hilfe",
    extrasDisclaimer: "Disclaimer",
    extrasImprint: "Impressum",

    url: {
      extrasHelp: "https://docs.coscine.de/de/metadata/generator/about/",
      extrasDisclaimer:
        "https://git.rwth-aachen.de/coscine/docs/public/terms/-/blob/master/PrivacyPolicy.md",
      extrasImprint: "https://www.coscine.de/imprint/",
    },

    theme: {
      toggle: "Farbschema wählen",

      light: "Hell",
      dark: "Dunkel (Beta)",
    },
  },

  ApplicationProfile: "Applikationsprofil",

  Action: "Aktion",
  Create: "Erstellen",
  Import: "Importieren",
  New: "Neu",
  Edit: "Editieren",
  Copy: "Kopieren",
  Extend: "Erweitern",
  NewVersion: "Neue Version",

  mustNotBeClosed:
    "Aktuelles @:{'ApplicationProfile'} darf nicht geschlossen sein",

  EnterToken: "Token hinzufügen",
  Search: "Suche",
  Editor: "Editor",
  Graph: "Graph",
  MetadataForm: "Metadatenformular",
  Preview: "Vorschau",
  RDF: "Code Ansicht",
  Download: "Code Herunterladen",
  DownloadComplete: "Code mit Abhängigkeiten Herunterladen",
  Save: "Speichern",

  Lang: "Sprache",
  DE: "Deutsch",
  EN: "Englisch",

  Cancel: "Abbrechen",
  Continue: "Weiter",
  Submit: "Senden",
  SendToReview: "Zum Review einsenden",

  true: "Ja",
  false: "Nein",

  CurrentlyDemo:
    "Diese Webseite befindet sich aktuell in der Entwicklung und sollte nur zu Testzwecken verwendet werden!",

  CurrentlyMockup:
    "Sie befinden sich aktuell auf der Mockup Seite des AIMS Frontends. Keine Änderungen sind persistent!",

  availableapplicationprofiles: "Verfügbare @:{'ApplicationProfile'}e",
  vocabulary: "Vokabularterme",
  detailview: "Detailansicht (Felder)",
  properties: "Feld Eigenschaften",

  searchAP: "Suche @:{'ApplicationProfile'}e",
  searchVoc: "Suche Vokabularterme",

  created: "Erstelldatum",
  createdDescription:
    "Wann das @:{'ApplicationProfile'} erstellt wurde (dcterms:created)",
  creator: "Ersteller",
  creatorDescription:
    "Person, welche das @:{'ApplicationProfile'} erstellt hat (dcterms:creator)",
  creatorFilterDescription: "Filtern Sie auf Basis des Erstellers.",
  dateFilters: "Datum-Filter",
  dateFiltersDescription: "Tragen Sie ein Minimum und Maximum Datum ein.",
  description: "Beschreibung",
  descriptionDescription: "Inhaltliche Beschreibung (dcterms:description)",
  discipline: "Disziplin",
  label: "Titel",
  labelDescription:
    "Frei gewählter Name des @:{'ApplicationProfile'} (dcterms:title)",
  node: "Abgeleitet von",
  nodeDescription:
    "Bei der Option \"Erweitern\" wird hier das zugrundeliegende @:{'ApplicationProfile'} angezeigt (sh:node)",
  license: "Lizenz",
  licenseDescription: "Definition der Lizenz (dcterms:license)",
  subject: "Disziplin",
  subjectDescription:
    "Bereich in dem das @:{'ApplicationProfile'} angesiedelt ist (dcterms:subject)",
  subjectFilterDescription: "Filtern Sie auf Basis der Disziplin.",
  targetClass: "Zielklasse",
  targetClassDescription:
    "URL des @:{'ApplicationProfile'}s (s.o.) kopieren oder Klasse einer Ontologie angeben (sh:targetClass)",
  closed: "Geschlossen",
  closedDescription:
    "Offene @:{'ApplicationProfile'}e können erweitert werden, geschlossene @:{'ApplicationProfile'}e nicht. Beim Erweitern eines @:{'ApplicationProfile'}s wird ein neues erstellt, das das vorhandene erbt. (sh:closed)",
  url: "URL des @:{'ApplicationProfile'}s",
  urlDescription:
    "Auswählbar, ein eindeutiger Name ist für das @:{'ApplicationProfile'} anzugeben.",
  state: "Status des @:{'ApplicationProfile'}s",
  stateDescription:
    "Legen Sie fest, ob das @:{'ApplicationProfile'} noch als Draft vorliegen soll oder veröffentlicht (nicht mehr änderbar) werden kann.",
  stateFilterDescription: "Filtern Sie auf Basis des Status.",
  draft: "Entwurf",
  publish: "Veröffentlicht",
  necessaryBaseUrl: "Die Basis-URL '{base_url}' ist notwendig.",

  metadataForm: "Metadaten-Formular für {ap}",
  metadataView: "Metadaten-Ansicht",

  storeModalTitle: "Neues @:{'ApplicationProfile'} speichern: {apName}",
  editModalTitle: "@:{'ApplicationProfile'}e editieren: {apName}",

  defaultStoreToastDescription:
    "Ihr @:{'ApplicationProfile'} wurde gespeichert!",
  defaultStoreToastTitle: "@:{'ApplicationProfile'} gespeichert",

  defaultStoreFailedToastDescription:
    "Die Speicherung Ihres @:{'ApplicationProfile'}s ist fehlgeschlagen",
  defaultStoreFailedToastTitle: "Speicherung fehlgeschlagen",

  apiError: {
    unauthorized: {
      title: "Ein Fehler ist aufgetreten",
      body: "Authentifizierung fehlgeschlagen: Der Token im Authorization-Header ist ungültig, abgelaufen oder fehlt. Bitte einen gültigen Bearer Token bereitstellen.",
    },
    general: {
      title: "Ein Fehler ist aufgetreten",
      body: "Es ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Falls der Fehler weiterhin auftritt, wenden Sie sich bitte an Ihre Organisation.",
    },
    specific: {
      title: "Ein Fehler ist aufgetreten",
      body: "Es ist ein Fehler aufgetreten {error}. Bitte versuchen Sie es erneut. Falls der Fehler weiterhin auftritt, wenden Sie sich bitte an Ihre Organisation und geben Sie die folgende Fehler-ID an: {traceId}.",
    },
  },

  defaultStoringToastDescription: "Speichert ...",
  defaultStoringToastTitle: "Speichert @:{'ApplicationProfile'}",

  defaultStoreMetadataToastDescription: "Ihre Metadaten wurden gespeichert!",
  defaultStoreMetadataToastTitle: "Metadaten gespeichert",

  defaultStoreMetadataFailedToastDescription:
    "Die Speicherung Ihrer Metadaten ist fehlgeschlagen",
  defaultStoreMetadataFailedToastTitle: "Speicherung fehlgeschlagen",

  defaultStoringMetadataToastDescription: "Speichert ...",
  defaultStoringMetadataToastTitle: "Speichert Metadaten",

  emptyApplicationProfileList:
    "Es wurde kein passendes @:{'ApplicationProfile'} zu dieser Eingabe gefunden.",

  emptyVocabList:
    "Es wurde kein passender Vokabularterm zu dieser Eingabe gefunden.",
  addCustomTerm: "Benutzerdefinierten Term hinzufügen",

  currentAP: "Aktuelles @:{'ApplicationProfile'}",
  inheritedAPs: "Geerbte @:{'ApplicationProfile'}e",
  noNameSet: "(Kein Name Gesetzt)",
  emptyPathList:
    "Es wurde noch kein Vokabularterm zu diesem @:{'ApplicationProfile'} hinzugefügt. Sie können einen Vokabularterm hinzufügen, indem Sie einen Term aus der \"@:{'vocabulary'}\" Spalte in dieses @:{'ApplicationProfile'} ziehen.",

  pleaseSelectOption: "-- Bitte wählen Sie eine Option --",

  searchSelectNoOptions:
    "Es existieren keine auswählbaren Optionen. Sie können entweder eine spezifischere Abfrage eingeben oder eine eigene Option hinzufügen, indem Sie sie eintragen und den 'Enter' Key betätigen.",

  propertyTypesLabel: "Eigenschaftstyp",
  propertyTypesDescription: 'Art der Eigenschaft - Standardmäßig "Datentyp"',

  addEntry: "Eintrag hinzufügen",

  actions: {
    copyModalTitle: "@:{'ApplicationProfile'} Kopieren",
    copyModalDescription:
      "Diese Aktion wird ein neues @:{'ApplicationProfile'} erstellen, welches die Werte des vorher ausgewählten @:{'ApplicationProfile'}s kopiert. Wenn Sie gerade ein @:{'ApplicationProfile'} entwickeln, speichert der Klick auf 'Weiter' Ihren aktuellen Fortschritt in einem Entwurf.",
    createModalTitle: "Neues @:{'ApplicationProfile'} Erstellen",
    createModalDescription:
      "Diese Aktion erstellt ein neues @:{'ApplicationProfile'}. Wenn Sie gerade ein @:{'ApplicationProfile'} entwickeln, speichert der Klick auf 'Weiter' Ihren aktuellen Fortschritt in einem Entwurf.",
    extendModalTitle: "@:{'ApplicationProfile'} Erweitern",
    extendModalDescription:
      "Diese Aktion erstellt ein neues @:{'ApplicationProfile'}, welches das vorher ausgewählte @:{'ApplicationProfile'} erweitert. Wenn Sie gerade ein @:{'ApplicationProfile'} entwickeln, speichert der Klick auf 'Weiter' Ihren aktuellen Fortschritt in einem Entwurf.",
    importModalTitle: "@:{'ApplicationProfile'} Importieren",
    importModalDescription:
      "Diese Aktion importiert ein @:{'ApplicationProfile'}. Bitte drücken Sie auf den unteren Button und wählen Sie ihr @:{'ApplicationProfile'} aus. Wenn Sie gerade ein @:{'ApplicationProfile'} entwickeln, speichert der Klick auf 'Weiter' Ihren aktuellen Fortschritt in einem Entwurf.",
    importModalDropPlaceholder: "@:{'ApplicationProfile'} hierein ziehen...",
    importModalFilePlaceholder:
      "Wählen Sie ein @:{'ApplicationProfile'} aus oder ziehen Sie es hierein...",
    newVersionModalTitle: "Neue Version @:{'ApplicationProfile'} Erstellen",
    newVersionModalDescription:
      "Diese Aktion erstellt eine neue Version von dem vorher ausgewählten @:{'ApplicationProfile'}. Wenn Sie gerade ein @:{'ApplicationProfile'} entwickeln, speichert der Klick auf 'Weiter' Ihren aktuellen Fortschritt in einem Entwurf.",
    removeDraftModalTitle: "Entwurf Löschen",
    removeDraftModalDescription:
      'Diese Aktion wird den Entwurf "{name}" löschen.',
  },

  administrativeVisible: "Zeige Administrative Eigenschaften",
  propertyPairVisible: "Zeige Eigenschaften Zu Eigenschaftspaaren",
  qualifiedVisible:
    "Zeige Eigenschaften Zu Bedingten @:{'ApplicationProfile'}en",
  stringVisible: "Zeige String-basierte Eigenschaften",
  valueVisible: "Zeige Wert-basierte Eigenschaften",

  inheritanceVisible: "Zeige Verbindungen",

  shacl: {
    class: {
      label: "Klasse",
      description:
        "Eingegebener Wert muss eine Instanz der Klasse sein, z.B. http://purl.org/dc/dcmitype/ (sh:class)",
    },
    datatype: {
      label: "Datentyp",
      description:
        "Datentyp/-format der erwarteten Eingabewerte (Default: string) auf Basis der XML Schema Datatypes (sh:datatype)",
    },
    defaultValue: {
      label: "Standardwert",
      description: "Standardmäßig im Feld vorgegebener Wert (sh:defaultValue)",
    },
    description: {
      label: "Beschreibung",
      description: "Beschreibung der Eigenschaft (sh:description)",
    },
    disjoint: {
      label: "Ungleich",
      description:
        "Die Eigenschaft, zu welchem der gegebene Wert ungleich sein soll (sh:disjoint)",
    },
    equals: {
      label: "Gleich",
      description:
        "Die Eigenschaft, zu welchem der gegebene Wert gleich sein soll (sh:equals)",
    },
    flags: {
      label: "Regex Flags",
      description: "Regex flags (sh:flags)",
    },
    hasValue: {
      label: "Wert",
      description: "Expliziter erwarteter Wert (sh:hasValue)",
    },
    in: {
      label: "Liste",
      description:
        "Liste von String-Werten, z.B. Apfel, Banane, Orange (sh:in)",
    },
    lessThan: {
      label: "Kleiner",
      description:
        "Die Eigenschaft, zu welchem der gegebene Wert kleiner sein soll (sh:lessThan)",
    },
    lessThanOrEquals: {
      label: "Kleiner Oder Gleich",
      description:
        "Die Eigenschaft, zu welchem der gegebene Wert kleiner oder gleich sein soll (sh:lessThanOrEquals)",
    },
    maxCount: {
      label: "Maximale Mögliche Einträge",
      description:
        "Anzahl der Werte, die maximal vom Nutzer angegeben werden können (sh:maxCount)",
    },
    maxExclusive: {
      label: "Maximum Exklusive",
      description:
        "Festlegen der Wertebereiche für den Term, Maximum Exklusive (sh:maxExclusive)",
    },
    maxInclusive: {
      label: "Maximum Inklusive",
      description:
        "Festlegen der Wertebereiche für den Term, Maximum Inklusive (sh:maxInclusive)",
    },
    maxLength: {
      label: "Maximale Länge",
      description: "Maximales Vorkommen des Wertes (sh:maxLength)",
    },
    message: {
      label: "Fehlermeldung",
      description:
        "Fehlermeldung, die bei einem generellen Fehler ausgegeben wird (sh:message)",
    },
    minCount: {
      label: "Minimale Erforderliche Einträge",
      description:
        "Anzahl der Werte, die mindestens vom Nutzer angegeben werden müssen (sh:minCount",
    },
    minExclusive: {
      label: "Minimum Exklusive",
      description:
        'Festlegen der Wertebereiche für den Term, Minimum Exklusive (Beispiel: Min. Exlusive = "1" dann ist der Wert "1" nicht erlaubt) (sh:minExclusive)',
    },
    minInclusive: {
      label: "Minimum Inklusive",
      description:
        "Festlegen der Wertebereiche für den Term, Minimum Inklusive (sh:minInclusive)",
    },
    minLength: {
      label: "Minimale Länge",
      description: "Minimales Vorkommen des Wertes (sh:minLength)",
    },
    name: {
      label: "Feld Name",
      description: "Anzeigename des Feldes (sh:name)",
    },
    node: {
      label: "Erfüllt Profil (immer) (node)",
      description:
        "Zeigt auf ein anderes validierendes @:{'ApplicationProfile'}, z.B. https://purl.org/coscine/ap/engmeta (sh:node)",
    },
    nodeKind: {
      label: "Node Kind",
      description: "Aktuell nicht unterstützt (sh:nodeKind)",
    },
    order: {
      label: "Position Auf Metadaten-Formular",
      description:
        "Darstellungsreihenfolge der Eigenschaft unter den anderen Eigenschaften (sh:order)",
    },
    path: {
      label: "Term IRI",
      description: "Pfad/URL zur Definition des Terms (sh:path)",
    },
    pattern: {
      label: "Regex Pattern",
      description: "Regex Pattern zur Überprüfung des Wertes (sh:pattern)",
    },
    qualifiedMaxCount: {
      label: "Maximales Vorkommen des bedingten @:{'ApplicationProfile'}s",
      description:
        "Angabe des maximal gestatteten Vorkommen des bedingten @:{'ApplicationProfile'}s (sh:qualifiedMaxCount)",
    },
    qualifiedMinCount: {
      label: "Minimales Vorkommen des bedingten @:{'ApplicationProfile'}s",
      description:
        "Angabe des minimal gestatteten Vorkommen des bedingten @:{'ApplicationProfile'}s (sh:qualifiedMinCount)",
    },
    qualifiedValueShape: {
      label: "Erfüllt Profil (m-n Mal) (bedingt)",
      description:
        "URL/Definition eines bedingten @:{'ApplicationProfile'}s (sh:qualifiedValueShape)",
    },
    qualifiedValueShapesDisjoint: {
      label: "Bedingte @:{'ApplicationProfile'}e können disjunkt sein",
      description:
        "Ein-/Ausschalten ob @:{'ApplicationProfile'}e disjunkt sein können (sh:qualifiedValueShapesDisjoint)",
    },
    severity: {
      label: "Fehler-Schwere",
      description: "Die Schwere des Fehlers (sh:severity)",
    },
    singleLine: {
      label: "Einzeilig",
      description: "Einzeiligkeit des Metadatenfeldes (dash:singleLine)",
    },
  },

  communitiesLabel: "Communities",
  communitiesDescription:
    "Bitte wählen Sie die Community aus, für die Sie veröffentlichen wollen",
  communitiesFilterDescription:
    "Wählen Sie eine Community, zu denen ein @:{'ApplicationProfile'} zugehörig ist.",

  aims: {
    afterStoreModalTitle: "Ihr Zugangstoken",
    afterStoreModalDescription:
      "Sollten Sie Ihr @:{'ApplicationProfile'} überarbeiten oder veröffentlichen wollen, benötigen Sie einen Zugangstoken. Deswegen ist es wichtig, den folgenden Token zu notieren:",

    fundingStatement:
      "NFDI4Ing wird gefördert durch die DFG unter Projektnummer",

    reviewModalTitle: "@:{'ApplicationProfile'} zum Review einsenden",
    reviewModalDescription:
      "Bitte wählen Sie eine Community aus, zu der Sie das @:{'ApplicationProfile'} veröffentlichen wollen:",
    reviewedModalDescription:
      "Die Anfrage wird begutachtet. Sie können den Fortschritt unter der folgenden Merge Request verfolgen:",
  },

  coscine: {
    authorizationModalTitle: "Fügen Sie Ihren Bearer Token ein",
    storeDescription:
      "Sie werden eine Übermittlung Ihres benutzerdefinierten @:{'ApplicationProfile'}s beantragen. " +
      "Weiterhin werden Sie eine E-Mail mit einem Link zu Ihrer GitLab Merge Request erhalten, welche Sie über den Stand Ihres Antrags informiert.",
    storeToastDescription:
      "Es wurde Ihnen eine E-Mail geschickt. Sie werden über den Status Ihrer Anfrage informiert.",
    storeToastTitle: "@:{'ApplicationProfile'} übermittelt",
    tokenLabel: "Bearer Token",
    tokenHere: "hier",
    tokenNotice: "Sie können einen Token {0} generieren.",
    tokenUrl: "https://coscine.rwth-aachen.de/user/",
  },
};

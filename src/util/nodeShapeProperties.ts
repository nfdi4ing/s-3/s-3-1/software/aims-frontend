import type { NodeShapeProperty } from "@/types/shape-properties";
import { prefixes } from "@/util/linkedData";
import factory from "rdf-ext";
import { licenses, subjects } from "./shaclTypes";

export function getDefaultLocaleValue(locales: string[]): {
  [locale: string]: string;
} {
  const localeDefaultValue: { [locale: string]: string } = {};
  for (const localeEntry of locales) {
    localeDefaultValue[localeEntry] = "";
  }
  return localeDefaultValue;
}

export function retrieveNodeShapeProperties(
  locales: string[],
): NodeShapeProperty[] {
  const localeDefaultValue = getDefaultLocaleValue(locales);
  return [
    {
      localized: true,
      multiline: false,
      required: true,
      labelKey: "label",
      descriptionKey: "labelDescription",
      type: "text",
      predicate: factory.namedNode(prefixes.dcterms + "title"),
      value: { ...localeDefaultValue },
    },
    {
      localized: true,
      multiline: true,
      required: true,
      labelKey: "description",
      descriptionKey: "descriptionDescription",
      type: "text",
      predicate: factory.namedNode(prefixes.dcterms + "description"),
      value: { ...localeDefaultValue },
    },
    {
      localized: false,
      multiline: false,
      required: true,
      labelKey: "creator",
      descriptionKey: "creatorDescription",
      type: "text",
      predicate: factory.namedNode(prefixes.dcterms + "creator"),
      value: "",
    },
    {
      localized: false,
      multiline: false,
      required: true,
      labelKey: "created",
      descriptionKey: "createdDescription",
      type: "date",
      predicate: factory.namedNode(prefixes.dcterms + "created"),
      value: new Date().toISOString().slice(0, 10),
    },
    {
      localized: false,
      multiline: false,
      required: true,
      labelKey: "license",
      descriptionKey: "licenseDescription",
      items: licenses,
      type: "url",
      predicate: factory.namedNode(prefixes.dcterms + "license"),
      value: "http://spdx.org/licenses/CC0-1.0",
    },
    {
      labelKey: "subject",
      descriptionKey: "subjectDescription",
      predicate: factory.namedNode(prefixes.dcterms + "subject"),
      localized: false,
      items: subjects,
      type: "url",
      value: "",
    },
    {
      localized: false,
      multiline: false,
      labelKey: "node",
      descriptionKey: "nodeDescription",
      type: "list",
      listType: "regular",
      predicate: factory.namedNode(prefixes.sh + "node"),
      searchType: "node",
      value: [],
    },
    {
      localized: false,
      multiline: false,
      labelKey: "targetClass",
      descriptionKey: "targetClassDescription",
      type: "url",
      predicate: factory.namedNode(prefixes.sh + "targetClass"),
      searchType: "class",
      value: "",
    },
    {
      localized: false,
      multiline: false,
      labelKey: "closed",
      descriptionKey: "closedDescription",
      type: "boolean",
      predicate: factory.namedNode(prefixes.sh + "closed"),
      value: false,
    },
  ];
}

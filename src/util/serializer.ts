import stringifyStream from "stream-to-string";
import formats from "@rdfjs-elements/formats-pretty";
import prefixes from "@zazuko/prefixes/prefixes";
import type { Dataset } from "@rdfjs/types";

export async function serializeRDFDefinitionStandard(
  dataset: Dataset,
  contentType = "application/ld+json",
): Promise<string> {
  const canonical = dataset.toCanonical();
  const serializationPrefixes = {} as Record<string, string>;
  for (const prefix of Object.keys(prefixes)) {
    if (canonical.includes(prefixes[prefix])) {
      serializationPrefixes[prefix] = prefixes[prefix];
    }
  }
  const output = formats.serializers.import(contentType, dataset.toStream(), {
    prefixes: serializationPrefixes,
  });
  if (output === null) {
    return "";
  }
  return await stringifyStream(output as NodeJS.ReadableStream);
}

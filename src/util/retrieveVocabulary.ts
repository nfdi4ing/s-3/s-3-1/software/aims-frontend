import $rdf from "rdf-ext";

import type { Datasets } from "@zazuko/vocabularies/vocabularies";
import type { DataFactory, Quad } from "@rdfjs/types";

const vocabularyDatasets: Datasets = {};

const asDataset = (vocab: typeof import("@vocabulary/dbo")) => {
  return $rdf.dataset(
    vocab.default({ factory: $rdf as unknown as DataFactory<Quad, Quad> }),
  );
};

export const retrieveVocabulary = async (prefixes: string[]) => {
  for (const prefix of prefixes) {
    if (!vocabularyDatasets[prefix]) {
      switch (prefix) {
        case "dbo": {
          vocabularyDatasets[prefix] = asDataset(
            await import("@vocabulary/dbo"),
          );

          break;
        }
        case "dc11": {
          vocabularyDatasets[prefix] = asDataset(
            await import("@vocabulary/dc11"),
          );

          break;
        }
        case "dcat": {
          vocabularyDatasets[prefix] = asDataset(
            await import("@vocabulary/dcat"),
          );

          break;
        }
        case "dcterms": {
          vocabularyDatasets[prefix] = asDataset(
            await import("@vocabulary/dcterms"),
          );

          break;
        }
        case "foaf": {
          vocabularyDatasets[prefix] = asDataset(
            await import("@vocabulary/foaf"),
          );

          break;
        }
        case "m4i": {
          vocabularyDatasets[prefix] = asDataset(
            await import("@vocabulary/m4i"),
          );

          break;
        }
        case "schema": {
          vocabularyDatasets[prefix] = asDataset(
            await import("@vocabulary/schema"),
          );

          break;
        }
        // No default
      }
    }
  }
  return vocabularyDatasets;
};

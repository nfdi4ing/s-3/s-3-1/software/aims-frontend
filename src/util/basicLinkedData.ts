import factory from "rdf-ext";
import type { Dataset, Quad } from "@rdfjs/types";
import { Readable } from "node:stream";
import { rdfParser } from "rdf-parse";
import stringifyStream from "stream-to-string";
import formats from "@rdfjs-elements/formats-pretty";
import zazukoPrefixes from "@zazuko/prefixes/prefixes";

export async function parseRDFDefinition(
  definition: string,
  contentType = "application/ld+json",
  baseIRI = "http://aims.org",
): Promise<Dataset> {
  const input = new Readable({
    read: () => {
      input.push(definition);
      // eslint-disable-next-line unicorn/no-array-push-push
      input.push(null);
    },
  });
  const dataset = factory.dataset();
  await new Promise((resolve) => {
    rdfParser
      .parse(input, { contentType: contentType, baseIRI: baseIRI })
      .on("data", (quad: Quad) => dataset.add(quad))
      .on("error", (error: unknown) => console.error(error))
      .on("end", () => resolve(dataset));
  });
  return dataset as unknown as Dataset;
}

export async function serializeRDFDefinition(
  dataset: Dataset,
  contentType = "application/ld+json",
): Promise<string> {
  const canonical = dataset.toCanonical();
  if (!canonical) {
    return "";
  }
  const serializationPrefixes: Record<string, string> = {};
  for (const prefix of Object.keys(zazukoPrefixes)) {
    if (canonical.includes(zazukoPrefixes[prefix])) {
      serializationPrefixes[prefix] = zazukoPrefixes[prefix];
    }
  }
  const output = formats.serializers.import(contentType, dataset.toStream(), {
    prefixes: serializationPrefixes,
  });
  if (output === null) {
    return "";
  }
  return await stringifyStream(output as NodeJS.ReadableStream);
}

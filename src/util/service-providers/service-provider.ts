import AIMSServiceProvider from "@/util/service-providers/aims/aims-service-provider";
import DemoAIMSServiceProvider from "@/util/service-providers/aims/demo-aims-service-provider";
import CoscineServiceProvider from "@/util/service-providers/coscine/coscine-service-provider";
import MockupServiceProvider from "@/util/service-providers/mockup/mockup-service-provider";
import ServiceProvider from "@/util/service-providers/interfaces/service-provider";

const serviceProviders: ServiceProvider[] = [
  new AIMSServiceProvider(),
  new DemoAIMSServiceProvider(),
  new CoscineServiceProvider(),
];

let serviceProvider: ServiceProvider | undefined = serviceProviders.find(
  (serviceProvider) => serviceProvider.applicable(),
);

if (serviceProvider === undefined) {
  serviceProvider = new MockupServiceProvider();
}

const returnServiceProvider: ServiceProvider = serviceProvider;
export default returnServiceProvider;

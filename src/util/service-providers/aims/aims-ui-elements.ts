import AimsAfterStoreModal from "@/components/ServiceProviders/AIMS/AimsAfterStoreModal.vue";
import AimsFooter from "@/components/ServiceProviders/AIMS/AimsFooter.vue";
import AimsHeaderBanner from "@/components/ServiceProviders/AIMS/AimsHeaderBanner.vue";
import AimsReviewModal from "@/components/ServiceProviders/AIMS/AimsReviewModal.vue";

import type CustomUIElements from "@/util/service-providers/interfaces/custom-ui-elements";

export const standardUiElements: CustomUIElements = {
  AfterStoreModal: AimsAfterStoreModal,
  CustomFooter: AimsFooter,
  ReviewModal: AimsReviewModal,
};

export const demoUiElements: CustomUIElements = {
  ...standardUiElements,
  CustomHeaderBanner: AimsHeaderBanner,
};

import AIMSApiConnection from "@/util/service-providers/aims/aims-api-connection";
import AIMSServiceProvider from "@/util/service-providers/aims/aims-service-provider";
import { demoUiElements } from "./aims-ui-elements";

const base_url = "https://w3id.org/nfdi4ing/";

class DemoAIMSServiceProvider extends AIMSServiceProvider {
  apiConnection = new AIMSApiConnection(
    base_url,
    "https://pg4aims-test.ulb.tu-darmstadt.de/AIMS",
    "https://api.aims.otc.coscine.dev",
    "https://service.tib.eu/ts4tib",
  );
  applicable(): boolean {
    return window.location.hostname.includes("demo.aims-projekt");
  }
  uiElements = demoUiElements;
}

export default DemoAIMSServiceProvider;

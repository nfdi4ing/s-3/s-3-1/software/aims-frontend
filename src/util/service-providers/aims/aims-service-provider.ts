import type ServiceProvider from "@/util/service-providers/interfaces/service-provider";
import AIMSApiConnection from "@/util/service-providers/aims/aims-api-connection";
import aimsLogo from "@/assets/AIMS-Logo-2-Transparent.png";
import { standardUiElements } from "./aims-ui-elements";

const base_url = "https://w3id.org/nfdi4ing/";

class AIMSServiceProvider implements ServiceProvider {
  apiConnection = new AIMSApiConnection(
    base_url,
    "https://pg4aims.ulb.tu-darmstadt.de/AIMS",
    "https://api.aims.otc.coscine.dev",
    "https://service.tib.eu/ts4tib",
  );
  base_url = base_url;
  base_path = "profiles/";
  can_store_metadata = true;
  curation_process = true;
  filtered_search = true;
  force_base_url = true;
  logo = {
    class: "aimsLogo",
    location: aimsLogo,
  };
  messages = {
    de: {
      ApplicationProfile: "Metadatenprofil",
      Submit: "Speichern",
      nav: {
        url: {
          extrasHelp:
            "https://aims.pages.rwth-aachen.de/public/documentation/user-guide/platform/about/",
          extrasDisclaimer:
            "https://www.tu-darmstadt.de/datenschutzerklaerung.de.jsp",
          extrasImprint:
            "https://www.ulb.tu-darmstadt.de/impressum/index.de.jsp",
        },
      },
    },
    en: {
      ApplicationProfile: "Metadata Profile",
      nav: {
        url: {
          extrasHelp:
            "https://aims.pages.rwth-aachen.de/public/documentation/user-guide/platform/about/",
          extrasDisclaimer:
            "https://www.tu-darmstadt.de/datenschutzerklaerung.de.jsp",
          extrasImprint:
            "https://www.ulb.tu-darmstadt.de/impressum/index.de.jsp",
        },
      },
    },
  };
  name = "AIMS";
  uiElements = standardUiElements;

  applicable(): boolean {
    return window.location.hostname.includes("nfdi4ing");
  }
  authorized(): boolean {
    return true;
  }
  getToken(): string {
    return "";
  }
}

export default AIMSServiceProvider;

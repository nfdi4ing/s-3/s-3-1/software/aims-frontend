import type ApiConnection from "@/util/service-providers/interfaces/api-connection";
import type CustomUIElements from "@/util/service-providers/interfaces/custom-ui-elements";
import type locales from "@/locale/locales";

type DeepPartial<T> = T extends object
  ? {
      [P in keyof T]?: DeepPartial<T[P]>;
    }
  : T;

declare interface ServiceProvider {
  apiConnection: ApiConnection;
  base_path?: string;
  base_url: string;
  can_store_metadata: boolean;
  curation_process?: boolean;
  filtered_search?: boolean;
  force_base_url: boolean;
  logo: {
    class: string;
    location: string;
  };
  /**
   * Every message in the AIMS frontend can be overwritten by a service provider
   */
  messages?: DeepPartial<typeof locales>;
  name: string;
  uiElements: CustomUIElements;

  applicable: () => boolean;
  authorized: () => boolean;
  getToken: () => string;
}

export = ServiceProvider;

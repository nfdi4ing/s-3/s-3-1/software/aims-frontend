import type {
  ApplicationProfile,
  ApplicationProfileSearchParameters,
} from "@/types/application-profile";
import type VocabularyTerm from "@/types/vocabulary-term";
import type { Dataset } from "@rdfjs/types";

import TerminologyService from "@/util/api-connections/terminology-service";
import ApiConnection from "@/util/service-providers/interfaces/api-connection";

import useMockupStore from "./mockup-store";

import APs from "@/data/application-profiles.json";
import classes from "@/data/classes.json";
import vocabularyTerms from "@/data/vocabulary-terms.json";
import type { PredicateObject } from "../aims/metadata";
import type { Community } from "../interfaces/labels";

class MockupApiConnection implements ApiConnection {
  terminologyService!: TerminologyService;

  constructor(
    base_url?: string,
    terminologyServiceEndpoint = "https://service.tib.eu/ts4tib",
  ) {
    this.terminologyService = new TerminologyService(
      terminologyServiceEndpoint,
      base_url,
    );
  }

  async getApplicationProfile(base_url: string): Promise<ApplicationProfile> {
    const dataResponse = APs.find((ap) => ap.base_url === base_url);
    if (dataResponse !== undefined) {
      return dataResponse;
    }
    const mockupResponse = useMockupStore().getApplicationProfile(base_url);
    if (mockupResponse !== undefined) {
      return mockupResponse;
    }
    throw new Error("Base Url does not exist");
  }

  async getCommunities(): Promise<Community[]> {
    return [
      { url: "AIMS", name: "AIMS" },
      { url: "NFDI4Ing", name: "NFDI4Ing" },
    ];
  }

  async searchAllApplicationProfiles(
    query: string,
    searchParameters: ApplicationProfileSearchParameters,
  ): Promise<Array<ApplicationProfile>> {
    return await this.searchApplicationProfiles(query, searchParameters);
  }

  async searchApplicationProfiles(
    query: string,
    searchParameters: ApplicationProfileSearchParameters,
  ): Promise<Array<ApplicationProfile>> {
    const queryFilter = query.toLowerCase().trim();
    return [
      ...APs.filter(
        (ap) =>
          (ap.name.toLowerCase().trim().includes(queryFilter) ||
            ap.base_url.toLowerCase().trim().includes(queryFilter)) &&
          (!searchParameters.community ||
            ap.approvedBy?.includes(searchParameters.community) === true),
      ),
      ...useMockupStore().searchApplicationProfiles(query, searchParameters),
    ];
  }

  async searchClasses(query: string): Promise<Array<VocabularyTerm>> {
    const queryFilter = query.toLowerCase().trim();
    return classes.filter((vocTerm) =>
      vocTerm.uri.toLowerCase().trim().includes(queryFilter),
    );
  }

  async searchMetadata(
    query: string,
    targetClass: string | null,
    predicateObjects: Array<PredicateObject>,
  ): Promise<Dataset[]> {
    return await useMockupStore().getMetadata(
      query,
      targetClass,
      predicateObjects,
    );
  }

  async searchVocabularyTerms(query: string): Promise<Array<VocabularyTerm>> {
    const queryFilter = query.toLowerCase().trim();
    return vocabularyTerms.filter((vocTerm) =>
      vocTerm.uri.toLowerCase().trim().includes(queryFilter),
    );
  }

  async storeApplicationProfile(
    applicationProfile: ApplicationProfile,
    dataset: Dataset,
  ): Promise<ApplicationProfile | void> {
    useMockupStore().storeApplicationProfile(applicationProfile, dataset);
  }

  async storeMetadata(
    _: ApplicationProfile,
    metadata: Dataset,
  ): Promise<boolean> {
    await useMockupStore().storeMetadata(metadata);
    return true;
  }
}

export default MockupApiConnection;

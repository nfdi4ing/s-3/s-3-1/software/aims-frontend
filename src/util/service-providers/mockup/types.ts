import type { RemovableRef } from "@vueuse/core";
import type { ApplicationProfile } from "@/types/application-profile";
import type { Dataset } from "@rdfjs/types";

export interface MockupState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  aims: {
    mockup: {
      applicationProfiles: RemovableRef<Record<string, ApplicationProfile>>;
      metadata: RemovableRef<string[]>;
      parsedMetadata: Dataset[];
    };
  };
}

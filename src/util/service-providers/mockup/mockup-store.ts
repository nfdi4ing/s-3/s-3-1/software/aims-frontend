import { useLocalStorage } from "@vueuse/core";
import { defineStore } from "pinia";

import type {
  ApplicationProfile,
  ApplicationProfileSearchParameters,
} from "@/types/application-profile";
import type { Dataset } from "@rdfjs/types";

import type { MockupState } from "./types";

import State from "@/types/state";
import prefixes from "@zazuko/prefixes/prefixes";
import factory from "rdf-ext";
import { parseRDFDefinition } from "@/util/basicLinkedData";
import type { PredicateObject } from "../aims/metadata";

/*  
  Store variable name is "this.<id>Store"
    id: "mockup" --> this.mockupStore
  Important! The id must be unique for every store.
*/
export const useMockupStore = defineStore({
  id: "mockup",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): MockupState => ({
    aims: {
      mockup: {
        applicationProfiles: useLocalStorage<
          Record<string, ApplicationProfile>
        >("aims.mockup.applicationProfiles", {}),
        metadata: useLocalStorage<string[]>("aims.mockup.metadata", []),
        parsedMetadata: [] as Dataset[],
      },
    },
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.mainStore.<getter_name>;
  */
  getters: {
    applicationProfiles(): Record<string, ApplicationProfile> {
      return this.aims.mockup.applicationProfiles;
    },
    metadata(): Dataset[] {
      return this.aims.mockup.parsedMetadata;
    },
  },
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.mainStore.<action_name>();
  */
  actions: {
    getApplicationProfile(baseUrl: string): ApplicationProfile | undefined {
      if (this.applicationProfiles[baseUrl]) {
        return this.applicationProfiles[baseUrl];
      }
      return undefined;
    },
    removeApplicationProfile(applicationProfile: ApplicationProfile) {
      const drafts = { ...this.applicationProfiles };
      delete drafts[applicationProfile.base_url];
      this.aims.mockup.applicationProfiles = drafts;
    },
    searchApplicationProfiles(
      query: string,
      searchParameters: ApplicationProfileSearchParameters,
    ): ApplicationProfile[] {
      const queryFilter = query.toLowerCase().trim();
      return Object.values(this.applicationProfiles).filter(
        (ap) =>
          (ap.name.toLowerCase().trim().includes(queryFilter) ||
            ap.base_url.toLowerCase().trim().includes(queryFilter)) &&
          (!searchParameters.community ||
            ap.approvedBy?.includes(searchParameters.community) === true),
      );
    },
    storeApplicationProfile(
      applicationProfile: ApplicationProfile,
      dataset: Dataset,
    ) {
      const drafts = { ...this.applicationProfiles };
      applicationProfile.definition = dataset.toCanonical();
      applicationProfile.mimeType = "application/n-quads";
      applicationProfile.state = State.PUBLISHED;
      drafts[applicationProfile.base_url] = applicationProfile;
      this.aims.mockup.applicationProfiles = drafts;
    },

    async getMetadata(
      query: string,
      targetClass: string | null,
      predicateObjects: Array<PredicateObject>,
    ): Promise<Dataset[]> {
      if (this.metadata.length !== this.aims.mockup.metadata.length) {
        this.aims.mockup.parsedMetadata = await this.parseMetadata(
          this.aims.mockup.metadata,
        );
      }
      const metadata: Dataset[] = [];
      const lowerCaseQuery = query.toLowerCase().trim();
      let searchableMetadata = this.metadata;
      if (targetClass) {
        searchableMetadata = searchableMetadata.filter(
          (entry) =>
            [
              ...entry.match(
                null,
                factory.namedNode(prefixes.rdf + "type"),
                factory.namedNode(targetClass),
              ),
            ].length > 0,
        );
      }

      for (const predicateObject of predicateObjects) {
        searchableMetadata = searchableMetadata.filter((entry) =>
          entry.some(
            (quad) =>
              predicateObject.predicate !== undefined &&
              quad.predicate.value.includes(predicateObject.predicate) &&
              predicateObject.object !== undefined &&
              quad.object.value.includes(predicateObject.object),
          ),
        );
      }

      for (const dataset of searchableMetadata) {
        if (
          dataset.some((quad) =>
            quad.object.value.toLowerCase().trim().includes(lowerCaseQuery),
          )
        ) {
          metadata.push(dataset);
        }
      }
      return metadata;
    },
    async parseMetadata(metadata: string[]) {
      const parsedMetadata: Dataset[] = [];
      for (const entry of metadata) {
        parsedMetadata.push(
          await parseRDFDefinition(entry, "application/n-quads"),
        );
      }
      return parsedMetadata;
    },
    async removeMetadata(index: number) {
      const metadata = [...this.aims.mockup.metadata];
      metadata.splice(index);
      this.aims.mockup.metadata = metadata;
      this.aims.mockup.parsedMetadata = await this.parseMetadata(
        this.aims.mockup.metadata,
      );
    },
    async storeMetadata(dataset: Dataset) {
      const metadata = [...this.aims.mockup.metadata];
      metadata.push(dataset.toCanonical());
      this.aims.mockup.metadata = metadata;
      this.aims.mockup.parsedMetadata = await this.parseMetadata(
        this.aims.mockup.metadata,
      );
    },
  },
});

export default useMockupStore;

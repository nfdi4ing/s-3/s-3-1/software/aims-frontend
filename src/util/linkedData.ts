import factory from "rdf-ext";
import type { ApplicationProfile } from "@/types/application-profile";
import ApplicationProfileImpl from "@/classes/ApplicationProfileImpl";
import type { NodeShapeProperty } from "@/types/shape-properties";
import type VocabularyTerm from "@/types/vocabulary-term";
import stringifyStream from "stream-to-string";
import type {
  Dataset,
  Literal,
  Quad,
  Quad_Object,
  Quad_Predicate,
  Quad_Subject,
} from "@rdfjs/types";
import formats, { mediaTypes } from "@rdfjs-elements/formats-pretty";

import { v4 as uuidv4 } from "uuid";

import { parseRDFDefinition } from "@/util/basicLinkedData";
import serviceProvider from "@/util/service-providers/service-provider";
import { getPrefixes } from "@/util/prefixes";

import { useMainStore } from "@/store";
import { type SelectionType, defaultDataType } from "./shaclTypes";

export const prefixes = getPrefixes(serviceProvider.base_url);

export function generateUri(prefix: string): string {
  return prefix + uuidv4() + "/";
}
export function generateTermUri(): string {
  return generateUri(prefixes.vocabterms);
}
export function generateAPUri(): string {
  return generateUri(prefixes.aps);
}

export async function getRDFContentTypes(): Promise<string[]> {
  return Object.values(mediaTypes);
}

export async function serializeRDFDefinition(
  dataset: Dataset,
  contentType = "application/ld+json",
): Promise<string> {
  const canonical = dataset.toCanonical();
  if (!canonical) {
    return "";
  }
  const serializationPrefixes: Record<string, string> = {};
  for (const prefix of Object.keys(prefixes)) {
    if (canonical.includes(prefixes[prefix])) {
      serializationPrefixes[prefix] = prefixes[prefix];
    }
  }
  const output = formats.serializers.import(contentType, dataset.toStream(), {
    prefixes: serializationPrefixes,
  });
  if (output === null) {
    return "";
  }
  return await stringifyStream(output as NodeJS.ReadableStream);
}

export async function parseApplicationProfile(
  applicationProfile: ApplicationProfile,
): Promise<Dataset> {
  if (applicationProfile.definition) {
    return await parseRDFDefinition(
      applicationProfile.definition,
      applicationProfile.mimeType,
      applicationProfile.base_url,
    );
  }
  return factory.dataset() as unknown as Dataset;
}

export async function parseApplicationProfiles(
  applicationProfiles: Array<ApplicationProfile>,
): Promise<Array<Dataset>> {
  const datasets: Dataset[] = [];
  for (const applicationProfile of applicationProfiles) {
    datasets.push(await parseApplicationProfile(applicationProfile));
  }
  return datasets;
}

export async function parseVocabularyTerm(
  vocabularyTerm: VocabularyTerm,
  contentType = "application/ld+json",
): Promise<Dataset> {
  return await parseRDFDefinition(
    JSON.stringify(vocabularyTerm.definition),
    contentType,
    vocabularyTerm.vocabulary,
  );
}

export async function parseVocabularyTerms(
  vocabularyTerms: Array<VocabularyTerm>,
  contentType = "application/ld+json",
): Promise<Array<Dataset>> {
  const datasets: Dataset[] = [];
  for (const vocabularyTerm of vocabularyTerms) {
    datasets.push(await parseVocabularyTerm(vocabularyTerm, contentType));
  }
  return datasets;
}

export function ensureApplicationProfileBaseUrl(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): void {
  if (applicationProfile.base_url === "") {
    const rootNode = factory.namedNode(generateAPUri());
    applicationProfile.base_url = rootNode.value;
  }
  const rootNode = factory.namedNode(applicationProfile.base_url);
  if (
    dataset.match(
      rootNode,
      factory.namedNode(prefixes.rdf + "type"),
      factory.namedNode(prefixes.sh + "NodeShape"),
    ).size === 0
  ) {
    dataset.add(
      factory.quad(
        rootNode,
        factory.namedNode(prefixes.rdf + "type"),
        factory.namedNode(prefixes.sh + "NodeShape"),
      ),
    );
  }
}

export function addVocabularyTermToDataset(
  vocabularyTerm: VocabularyTerm,
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): boolean {
  // Ensure Application Profile
  ensureApplicationProfileBaseUrl(applicationProfile, dataset);

  // Return false if the vocabulary term already exists in the ap
  if (
    dataset.some(
      (quad) =>
        quad.predicate.value == prefixes.sh + "path" &&
        quad.object.value == vocabularyTerm.uri,
    )
  ) {
    // This was disabled intentionally since there might be the case that multiple property paths for the same property exists
    // return false;
  }

  const blankNode = factory.blankNode();

  dataset.add(
    factory.quad(
      factory.namedNode(applicationProfile.base_url),
      factory.namedNode(prefixes.sh + "property"),
      blankNode,
    ),
  );

  dataset.add(
    factory.quad(
      blankNode,
      factory.namedNode(prefixes.sh + "path"),
      factory.namedNode(vocabularyTerm.uri),
    ),
  );

  if (
    vocabularyTerm.ranges &&
    vocabularyTerm.ranges.length > 0 &&
    !vocabularyTerm.ranges[0].includes(prefixes.rdfs) &&
    !vocabularyTerm.ranges[0].includes(prefixes.xsd)
  ) {
    dataset.add(
      factory.quad(
        blankNode,
        factory.namedNode(prefixes.sh + "class"),
        factory.namedNode(vocabularyTerm.ranges[0]),
      ),
    );
  } else {
    let datatype = prefixes.xsd + "string";
    if (
      vocabularyTerm.ranges &&
      vocabularyTerm.ranges.length > 0 &&
      vocabularyTerm.ranges[0].includes(prefixes.xsd)
    ) {
      datatype = vocabularyTerm.ranges[0];
    }
    dataset.add(
      factory.quad(
        blankNode,
        factory.namedNode(prefixes.sh + "datatype"),
        factory.namedNode(datatype),
      ),
    );
  }

  if (vocabularyTerm.label !== undefined) {
    dataset.add(
      factory.quad(
        blankNode,
        factory.namedNode(prefixes.sh + "name"),
        factory.literal(vocabularyTerm.label, "en"),
      ),
    );
    dataset.add(
      factory.quad(
        blankNode,
        factory.namedNode(prefixes.sh + "name"),
        factory.literal(vocabularyTerm.label, "de"),
      ),
    );
  }

  // Set order as default
  const orders = [...dataset]
    .filter((quad) => quad.predicate.value === prefixes.sh + "order")
    .map((quad) => Number(quad.object.value));
  const maxOrder = orders.length > 0 ? Math.max(...orders) : 0;

  dataset.add(
    factory.quad(
      blankNode,
      factory.namedNode(prefixes.sh + "order"),
      factory.literal(
        String(maxOrder + 1),
        factory.namedNode(prefixes.xsd + "integer"),
      ),
    ),
  );

  return true;
}

export function removeVocabularyTermFromDataset(
  vocabularyTermIdentifier: Quad_Subject,
  dataset: Dataset,
): void {
  dataset.deleteMatches(vocabularyTermIdentifier);
  dataset.deleteMatches(undefined, undefined, vocabularyTermIdentifier);
}

export function convertToSelectionType(object: Quad_Object): SelectionType {
  if (object.termType === "NamedNode") {
    return { text: "uri", value: "uri" };
  }
  if (object.termType === "Literal") {
    return {
      text: object.datatype.value.split("#")[1],
      value: object.datatype.value,
    };
  }
  return defaultDataType;
}

export function getObject(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  dataset: Dataset,
  locale?: string,
): Quad[] {
  let results = [...dataset.match(subject, predicate)];
  if (locale !== undefined) {
    results = results.sort((quad) =>
      (quad.object as Literal).language === locale ? -1 : 1,
    );
    if (results.length > 0) {
      return [results[0]];
    }
  }
  return results;
}

export function getObjectList(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  dataset: Dataset,
): Quad[] {
  let results = [...dataset.match(subject, predicate)];
  if (results.length > 0) {
    // Assume it is an rdf list and go through with it
    const quadList: Quad[] = [];
    let currentSubject = results[0].object;
    const rdfFirstNode = factory.namedNode(prefixes.rdf + "first");
    const rdfRestNode = factory.namedNode(prefixes.rdf + "rest");
    const rdfNilNode = factory.namedNode(prefixes.rdf + "nil");
    if (!currentSubject.equals(rdfNilNode)) {
      quadList.push(results[0]);
      do {
        results = [...dataset.match(currentSubject, rdfFirstNode)];
        if (results.length > 0) {
          quadList.push(results[0]);
        }
        results = [...dataset.match(currentSubject, rdfRestNode)];
        if (results.length > 0) {
          quadList.push(results[0]);
          currentSubject = results[0].object;
        } else {
          break;
        }
      } while (!currentSubject.equals(rdfNilNode));
    }
    return quadList;
  }
  return [];
}

export function getObjectStringList(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  dataset: Dataset,
): string[] {
  const rdfFirstNode = factory.namedNode(prefixes.rdf + "first");
  return getObjectList(subject, predicate, dataset)
    .filter((quad) => quad.predicate.equals(rdfFirstNode))
    .map((quad) => quad.object.value);
}

export function getObjectSelectionTypeList(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  dataset: Dataset,
): SelectionType[] {
  return getObject(subject, predicate, dataset).map((quad) =>
    convertToSelectionType(quad.object),
  );
}

export function addTripleIfNotEmpty(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  object: Quad_Object,
  dataset: Dataset,
): void {
  if (object.value !== undefined) {
    if (typeof object.value !== "string") {
      // @ts-expect-error: Will say value is never because it should be a string ... it is sometimes not though
      object.value = object.value + "";
    }
    if (object.value.trim() !== "") {
      dataset.add(factory.quad(subject, predicate, object));
    }
  }
}

export function updateObject(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  object: Quad_Object,
  dataset: Dataset,
): void {
  dataset.deleteMatches(subject, predicate);
  addTripleIfNotEmpty(subject, predicate, object, dataset);
}

export function updateObjects(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  objects: Quad_Object[],
  dataset: Dataset,
): void {
  dataset.deleteMatches(subject, predicate);
  for (const object of objects) {
    if (typeof object.value !== "string") {
      // @ts-expect-error: Will say value is never because it should be a string ... it is sometimes not though
      object.value = object.value + "";
    }
    dataset.add(factory.quad(subject, predicate, object));
  }
}

export function updateLocaleObject(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  object: Quad_Object,
  locale: string,
  dataset: Dataset,
): void {
  for (const match of dataset.match(subject, predicate)) {
    if ((match.object as Literal).language === locale) {
      dataset.delete(match);
    }
  }
  addTripleIfNotEmpty(subject, predicate, object, dataset);
}

export function updateListObject(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  objects: Quad_Object[],
  dataset: Dataset,
): void {
  const objectList = getObjectList(subject, predicate, dataset);
  for (const quad of objectList) {
    dataset.delete(quad);
  }
  let currentSubject = subject;
  let currentPredicate = predicate;
  let currentObject = factory.blankNode();

  const rdfFirstNode = factory.namedNode(prefixes.rdf + "first");
  const rdfRestNode = factory.namedNode(prefixes.rdf + "rest");
  const rdfNilNode = factory.namedNode(prefixes.rdf + "nil");

  for (const object of objects) {
    dataset.add(factory.quad(currentSubject, currentPredicate, currentObject));
    currentPredicate = rdfRestNode;
    currentSubject = currentObject;
    currentObject = factory.blankNode();
    if (typeof object.value !== "string") {
      // @ts-expect-error: Will say value is never because it should be a string ... it is sometimes not though
      object.value = object.value + "";
    }
    dataset.add(factory.quad(currentSubject, rdfFirstNode, object));
  }
  if (objects.length > 0) {
    dataset.add(factory.quad(currentSubject, currentPredicate, rdfNilNode));
  }
}

export function updateNodeImport(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): void {
  const comparerPredicates = new Set([
    prefixes.sh + "node",
    prefixes.sh + "qualifiedValueShape",
  ]);
  const applicationProfileBaseUrl = factory.namedNode(
    applicationProfile.base_url,
  );
  const owlImports = factory.namedNode(prefixes.owl + "imports");

  const existingNodes = [...dataset].filter((quad) =>
    comparerPredicates.has(quad.predicate.value),
  );
  const existingImports = [
    ...dataset.match(applicationProfileBaseUrl, owlImports),
  ];

  for (const existingImport of existingImports) {
    if (
      !existingNodes.some(
        (quad) => quad.object.value === existingImport.object.value,
      )
    ) {
      dataset.delete(existingImport);
    }
  }
  for (const existingNode of existingNodes) {
    if (
      !existingImports.some(
        (quad) => quad.object.value === existingNode.object.value,
      )
    ) {
      dataset.add(
        factory.quad(
          applicationProfileBaseUrl,
          owlImports,
          existingNode.object,
        ),
      );
    }
  }
}

export function getApplicationProfileProperty(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
  property: string,
): Quad_Object[] {
  const object = getObject(
    factory.namedNode(applicationProfile.base_url),
    factory.namedNode(property),
    dataset,
  );
  return object.map((entry) => entry.object);
}

export function getApplicationProfileLabel(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): Quad_Object[] {
  const dctermsTitle = getApplicationProfileProperty(
    applicationProfile,
    dataset,
    prefixes.dcterms + "title",
  );
  if (dctermsTitle.length > 0) {
    return dctermsTitle;
  }
  return getApplicationProfileProperty(
    applicationProfile,
    dataset,
    prefixes.rdfs + "label",
  );
}

export function getApplicationProfileLabelValue(
  applicationProfile: ApplicationProfile,
  definition: Dataset,
  currentLocale?: string,
): string {
  let label = getApplicationProfileLabelByLocale(
    applicationProfile,
    definition,
    currentLocale,
  );
  if (!label) {
    label = getApplicationProfileLabelByLocale(
      applicationProfile,
      definition,
      currentLocale === "en" ? "de" : "en",
    );
    if (!label) {
      return applicationProfile.base_url;
    }
  }
  return label;
}

export function getApplicationProfileLabelByLocale(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
  locale?: string,
): string {
  const labels = getApplicationProfileLabel(applicationProfile, dataset);
  for (const label of labels) {
    if (
      label.termType === "Literal" &&
      (!locale || label.language === locale)
    ) {
      return label.value;
    }
  }
  return "";
}

export function getApplicationProfileTargetClass(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): string {
  const objectQuad = getObject(
    factory.namedNode(applicationProfile.base_url),
    factory.namedNode(prefixes.sh + "targetClass"),
    dataset,
  );
  if (objectQuad.length > 0) {
    return objectQuad[0].object.value;
  }

  return "";
}

export function editNodeShapeProperties(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
  properties: NodeShapeProperty[],
  generalLocale: string,
): void {
  // Ensure Application Profile
  ensureApplicationProfileBaseUrl(applicationProfile, dataset);

  const baseUrlNode = factory.namedNode(applicationProfile.base_url);

  for (const property of properties) {
    const predicate = property.predicate;

    const objects: Quad_Object[] = [];
    if (property.type === "boolean") {
      objects.push(
        factory.literal(
          property.value.toString(),
          factory.namedNode(prefixes.xsd + "boolean"),
        ),
      );
    } else if (property.type === "date") {
      objects.push(
        factory.literal(
          property.value.toString(),
          factory.namedNode(prefixes.xsd + "date"),
        ),
      );
    } else if (property.type === "list" && Array.isArray(property.value)) {
      for (const value of property.value) {
        if (value !== "") {
          objects.push(factory.namedNode(value));
        }
      }
    } else if (property.type === "url") {
      objects.push(factory.namedNode(property.value.toString()));
    } else {
      if (property.localized) {
        for (const localeValue of Object.keys(property.value)) {
          if (property.value[localeValue]) {
            objects.push(
              factory.literal(property.value[localeValue], localeValue),
            );
          }
        }
      } else {
        objects.push(factory.literal(property.value.toString()));
      }
    }

    dataset.deleteMatches(baseUrlNode, predicate);

    if (property.value !== "") {
      for (const object of objects) {
        dataset.add(factory.quad(baseUrlNode, predicate, object));
      }
    }
  }

  applicationProfile.name = getApplicationProfileLabelValue(
    applicationProfile,
    dataset,
    generalLocale,
  );

  afterEditNodeShapeHandler(applicationProfile, dataset);
}

export function afterEditNodeShapeHandler(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): void {
  const baseUrlNode = factory.namedNode(applicationProfile.base_url);
  let closed = "false";
  for (const match of dataset.match(
    baseUrlNode,
    factory.namedNode(prefixes.sh + "closed"),
  )) {
    closed = match.object.value;
  }
  const shPathNode = factory.namedNode(prefixes.sh + "path");
  const rdfTypeNode = factory.namedNode(prefixes.rdf + "type");
  for (const match of dataset.match(null, shPathNode, rdfTypeNode)) {
    dataset.deleteMatches(match.subject);
    dataset.deleteMatches(baseUrlNode, undefined, match.subject);
  }
  if (closed === "true") {
    const blankNode = factory.blankNode();
    dataset.add(
      factory.quad(
        baseUrlNode,
        factory.namedNode(prefixes.sh + "property"),
        blankNode,
      ),
    );
    dataset.add(factory.quad(blankNode, shPathNode, rdfTypeNode));
  }

  // Set Base Url as Class if no targetClass is defined
  const classNode = factory.namedNode(prefixes.rdfs + "Class");
  if (
    dataset.match(baseUrlNode, factory.namedNode(prefixes.sh + "targetClass"))
      .size === 0
  ) {
    dataset.add(factory.quad(baseUrlNode, rdfTypeNode, classNode));
  } else {
    dataset.deleteMatches(baseUrlNode, rdfTypeNode, classNode);
  }
}

export function initAPWithName(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
  generalLocale: string,
) {
  const localeValue: { [locale: string]: string } = {};
  if (generalLocale === "en") {
    localeValue.en = new Date().toLocaleString("en-US", { hour12: false });
  } else if (generalLocale === "de") {
    localeValue.de = new Date().toLocaleString("de-de", { hour12: false });
  }

  editNodeShapeProperties(
    applicationProfile,
    dataset,
    [
      {
        localized: true,
        multiline: false,
        required: true,
        labelKey: "label",
        descriptionKey: "labelDescription",
        type: "text",
        predicate: factory.namedNode(prefixes.dcterms + "title"),
        value: localeValue,
      },
      {
        localized: false,
        multiline: false,
        required: true,
        labelKey: "created",
        descriptionKey: "createdDescription",
        type: "date",
        predicate: factory.namedNode(prefixes.dcterms + "created"),
        value: new Date().toISOString().slice(0, 10),
      },
      {
        localized: false,
        multiline: false,
        required: true,
        labelKey: "license",
        descriptionKey: "licenseDescription",
        type: "text",
        predicate: factory.namedNode(prefixes.dcterms + "license"),
        value: "http://spdx.org/licenses/CC0-1.0",
      },
    ],
    generalLocale,
  );
}

/**
 * This method has a use since by executing a copy or new version action,
 * the old provenance information is not relevant in the new ap.
 * Therefore, it gets removed before the action executes.
 */
export function removeOldProvenanceInformation(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): void {
  const baseUrlNode = factory.namedNode(applicationProfile.base_url);
  dataset.deleteMatches(
    baseUrlNode,
    factory.namedNode(prefixes.prov + "wasDerivedFrom"),
  );
  dataset.deleteMatches(
    baseUrlNode,
    factory.namedNode(prefixes.prov + "wasRevisionOf"),
  );
}

export function changeBaseUrl(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
  url: string,
  deleteOld = true,
): void {
  const mainStore = useMainStore();

  if (deleteOld) {
    mainStore.removeApplicationProfile(applicationProfile);
  }

  // Ensure Application Profile
  ensureApplicationProfileBaseUrl(applicationProfile, dataset);

  // Readd triples to ensure that the new graph url is set
  const newBaseUrlNode = factory.namedNode(url);
  const fullContent = dataset.match();
  dataset.deleteMatches();
  for (const triple of fullContent) {
    dataset.add(
      factory.quad(
        triple.subject.value === applicationProfile.base_url
          ? newBaseUrlNode
          : triple.subject,
        triple.predicate,
        triple.object,
      ),
    );
  }

  applicationProfile.base_url = url;

  mainStore.storeApplicationProfile(applicationProfile, dataset);
}

export async function setApplicationProfileBasedOnDataset(
  dataset: Dataset,
  locale?: string,
  oldBaseUrl?: string,
): Promise<ApplicationProfile> {
  const applicationProfile = new ApplicationProfileImpl("", "", {
    definition: await serializeRDFDefinition(dataset, "text/turtle"),
    mimeType: "text/turtle",
  });
  const baseUrls = dataset.match(
    null,
    factory.namedNode(prefixes.rdf + "type"),
    factory.namedNode(prefixes.sh + "NodeShape"),
  );
  if (baseUrls.size > 0) {
    for (const match of baseUrls) {
      applicationProfile.base_url = match.subject.value;
      break;
    }
  } else if (oldBaseUrl) {
    applicationProfile.base_url = oldBaseUrl;
  }
  ensureApplicationProfileBaseUrl(applicationProfile, dataset);

  applicationProfile.name = getApplicationProfileLabelValue(
    applicationProfile,
    dataset,
    locale,
  );

  return applicationProfile;
}

declare type CoscineType = {
  readonly authorization: { bearer: string };
  readonly clientcorrolation: { readonly id: string };
  readonly loading: { counter: number };
};

declare global {
  const coscine: CoscineType;
}

declare interface Window {
  coscine: CoscineType;
}

import { Buffer } from "buffer/";

globalThis.global = globalThis;
window.global = globalThis;
/* eslint-disable @typescript-eslint/no-explicit-any */
(global.Buffer as any) = Buffer;
(window.Buffer as any) = Buffer;
/* eslint-enable @typescript-eslint/no-explicit-any */

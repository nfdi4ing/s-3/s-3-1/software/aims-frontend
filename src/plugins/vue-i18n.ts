import { createI18n } from "vue-i18n";

import locales from "@/locale/locales";

import serviceProvider from "@/util/service-providers/service-provider";

let localeValue = "en";
if (navigator.language.includes("de")) {
  localeValue = "de";
}

export const i18n = createI18n({
  locale: localeValue,
  messages: locales,
  globalInjection: true,
  silentFallbackWarn: true,
  legacy: false,
});

if (serviceProvider.messages) {
  if (serviceProvider.messages.de) {
    i18n.global.mergeLocaleMessage("de", serviceProvider.messages.de);
  }
  if (serviceProvider.messages.en) {
    i18n.global.mergeLocaleMessage("en", serviceProvider.messages.en);
  }
}

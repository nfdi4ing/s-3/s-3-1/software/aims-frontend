# MetadataEditor

## Props

| Prop name                        | Description | Type                                                                                                                                                                           | Values | Default |
| -------------------------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------ | ------- |
| applicationProfile               |             | ApplicationProfile                                                                                                                                                             | -      |         |
| completeApplicationProfile       |             | {<br/> definition: Dataset;<br/> inheritedParts: Record&lt;<br/> string,<br/> {<br/> applicationProfile: ApplicationProfile;<br/> definition: Dataset;<br/> }<br/> &gt;;<br/>} | -      |         |
| definition                       |             | Dataset                                                                                                                                                                        | -      |         |
| sessionStoredApplicationProfiles |             | number                                                                                                                                                                         | -      | 0       |
| token                            |             | string                                                                                                                                                                         | -      | ""      |

## Events

| Event name               | Properties | Description |
| ------------------------ | ---------- | ----------- |
| emptyApplicationProfile  |            |
| selectApplicationProfile |            |

---

```vue live
<MetadataEditor
  :applicationProfile="Default Example Usage"
  :completeApplicationProfile="Default Example Usage"
  :definition="Default Example Usage"
/>
```

# HomeView

## Props

| Prop name                        | Description | Type                                                                                                                                                                           | Values | Default |
| -------------------------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------ | ------- |
| applicationProfile               |             | ApplicationProfile                                                                                                                                                             | -      |         |
| completeApplicationProfile       |             | {<br/> definition: Dataset;<br/> inheritedParts: Record&lt;<br/> string,<br/> {<br/> applicationProfile: ApplicationProfile;<br/> definition: Dataset;<br/> }<br/> &gt;;<br/>} | -      |         |
| currentApplicationProfile        |             | ApplicationProfile                                                                                                                                                             | -      |         |
| currentDefinition                |             | Dataset                                                                                                                                                                        | -      |         |
| currentReadonly                  |             | boolean                                                                                                                                                                        | -      | false   |
| definition                       |             | Dataset                                                                                                                                                                        | -      |         |
| interactive                      |             | boolean                                                                                                                                                                        | -      | true    |
| queryingApplicationProfile       |             | boolean                                                                                                                                                                        | -      | false   |
| readonly                         |             | boolean                                                                                                                                                                        | -      | false   |
| selectedProperty                 |             | Quad_Subject                                                                                                                                                                   | -      |         |
| sessionStoredApplicationProfiles |             | number                                                                                                                                                                         | -      | 0       |
| token                            |             | string                                                                                                                                                                         | -      | ""      |

## Events

| Event name                        | Properties                                     | Description |
| --------------------------------- | ---------------------------------------------- | ----------- |
| addVocabularyTerm                 |                                                |
| emptyApplicationProfile           |                                                |
| openEditModal                     |                                                |
| removeProperty                    |                                                |
| selectApplicationProfile          |                                                |
| selectProperty                    |                                                |
| switchDisplayedApplicationProfile |                                                |
| switchInteractivity               |                                                |
| textDefinitionUpdate              | **&lt;anonymous1&gt;** `undefined` - undefined |
| updateDefinition                  |                                                |

---

```vue live
<HomeView
  :applicationProfile="Default Example Usage"
  :completeApplicationProfile="Default Example Usage"
  :currentApplicationProfile="Default Example Usage"
  :currentDefinition="Default Example Usage"
  :definition="Default Example Usage"
  :selectedProperty="Default Example Usage"
/>
```

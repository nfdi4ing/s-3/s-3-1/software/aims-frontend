# PropertyRenderer

## Props

| Prop name          | Description | Type               | Values | Default |
| ------------------ | ----------- | ------------------ | ------ | ------- |
| applicationProfile |             | ApplicationProfile | -      |         |
| definition         |             | Dataset            | -      |         |
| entry              |             | ShaclProperty      | -      |         |
| readonly           |             | boolean            | -      |         |
| selectedProperty   |             | Quad_Subject       | -      |         |

## Events

| Event name       | Properties                                                                                                                                           | Description |
| ---------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| updateDefinition |                                                                                                                                                      |
| updateProperty   | **&lt;anonymous1&gt;** `undefined` - undefined<br/>**&lt;anonymous2&gt;** `undefined` - undefined<br/>**&lt;anonymous3&gt;** `undefined` - undefined |

---

```vue live
<PropertyRenderer
  :applicationProfile="Default Example Usage"
  :definition="Default Example Usage"
  :entry="Default Example Usage"
  :readonly="true"
  :selectedProperty="Default Example Usage"
/>
```

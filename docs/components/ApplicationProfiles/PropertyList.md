# PropertyList

## Props

| Prop name          | Description | Type               | Values | Default       |
| ------------------ | ----------- | ------------------ | ------ | ------------- |
| applicationProfile |             | ApplicationProfile | -      |               |
| givenEntry         |             | ShaclProperty      | -      |               |
| placeholder        |             | string             | -      | "Placeholder" |
| readonly           |             | boolean            | -      |               |
| value              |             | array              | -      |               |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| input      |            |
| change     |            |

---

```vue live
<PropertyList
  :applicationProfile="Default Example Usage"
  :givenEntry="Default Example Usage"
  :readonly="true"
  :modelValue="[1, 2, 3]"
/>
```

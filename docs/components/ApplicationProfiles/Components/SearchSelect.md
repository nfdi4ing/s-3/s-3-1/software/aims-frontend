# SearchSelect

## Props

| Prop name          | Description | Type               | Values          | Default |
| ------------------ | ----------- | ------------------ | --------------- | ------- |
| applicationProfile |             | ApplicationProfile | -               |         |
| readonly           |             | boolean            | -               | false   |
| type               |             | string             | `class`, `node` |         |
| value              |             | string             | -               | ""      |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| input      |            |

---

```vue live
<SearchSelect :applicationProfile="Default Example Usage" type="class" />
```

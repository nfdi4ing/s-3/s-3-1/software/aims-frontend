# ImportActionModal

## Props

| Prop name | Description | Type    | Values | Default        |
| --------- | ----------- | ------- | ------ | -------------- |
| open      |             | boolean | -      | () =&gt; false |

## Events

| Event name | Properties                                     | Description |
| ---------- | ---------------------------------------------- | ----------- |
| close      |                                                |
| save       | **&lt;anonymous1&gt;** `undefined` - undefined |

---

```vue live
<ImportActionModal :open="true" />
```

# MetadataForm

## Props

| Prop name                  | Description | Type                                                                                                                                                                           | Values | Default                                                                                                                 |
| -------------------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------ | ----------------------------------------------------------------------------------------------------------------------- |
| completeApplicationProfile |             | {<br/> definition: Dataset;<br/> inheritedParts: Record&lt;<br/> string,<br/> {<br/> applicationProfile: ApplicationProfile;<br/> definition: Dataset;<br/> }<br/> &gt;;<br/>} | -      |                                                                                                                         |
| metadata                   |             | Dataset                                                                                                                                                                        | -      |                                                                                                                         |
| readOnly                   |             | boolean                                                                                                                                                                        | -      | false                                                                                                                   |
| selectedApplicationProfile |             | ApplicationProfile                                                                                                                                                             | -      | () =&gt;<br/> new ApplicationProfileImpl("", "", {<br/> definition: "[]",<br/> mimeType: "application/ld+json",<br/> }) |
| selectedDefinition         |             | Dataset                                                                                                                                                                        | -      |                                                                                                                         |

## Events

| Event name  | Properties | Description |
| ----------- | ---------- | ----------- |
| isValid     |            |
| setMetadata |            |

---

```vue live
<MetadataForm
  :completeApplicationProfile="Default Example Usage"
  :metadata="Default Example Usage"
  :selectedDefinition="Default Example Usage"
/>
```

# ListVocs

## Props

| Prop name       | Description | Type             | Values | Default |
| --------------- | ----------- | ---------------- | ------ | ------- |
| readonly        |             | boolean          | -      | false   |
| searchRunning   |             | boolean          | -      | false   |
| vocabularyTerms |             | VocabularyTerm[] | -      |         |

## Events

| Event name        | Properties | Description |
| ----------------- | ---------- | ----------- |
| addVocabularyTerm |            |

---

```vue live
<ListVocs :vocabularyTerms="Default Example Usage" />
```

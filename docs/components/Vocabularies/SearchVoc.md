# SearchVoc

## Events

| Event name      | Properties | Description |
| --------------- | ---------- | ----------- |
| searching       |            |
| vocabularyTerms |            |

---

```vue live
<SearchVoc />
```

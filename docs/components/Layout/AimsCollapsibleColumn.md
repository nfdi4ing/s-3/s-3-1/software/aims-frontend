# AimsCollapsibleColumn

## Props

| Prop name   | Description | Type    | Values | Default |
| ----------- | ----------- | ------- | ------ | ------- |
| droppable   |             | boolean | -      | false   |
| isCollapsed |             | boolean | -      |         |
| size        |             | number  | -      |         |
| title       |             | string  | -      |         |

## Events

| Event name   | Properties | Description |
| ------------ | ---------- | ----------- |
| drop         |            |
| isCollapsing |            |

## Slots

| Name    | Description | Bindings |
| ------- | ----------- | -------- |
| default |             |          |

---

```vue live
<AimsCollapsibleColumn
  :isCollapsed="true"
  :size="42"
  title="Default Example Usage"
>Default Example Usage</AimsCollapsibleColumn>
```

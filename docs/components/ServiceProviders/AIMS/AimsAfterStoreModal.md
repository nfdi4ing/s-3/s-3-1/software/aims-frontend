# AimsAfterStoreModal

## Props

| Prop name          | Description | Type    | Values | Default        |
| ------------------ | ----------- | ------- | ------ | -------------- |
| lastAccessTokenUrl |             | string  | -      | () =&gt; ""    |
| open               |             | boolean | -      | () =&gt; false |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| close      |            |
| save       |            |

---

```vue live
<AimsAfterStoreModal lastAccessTokenUrl="Default Example Usage" :open="true" />
```

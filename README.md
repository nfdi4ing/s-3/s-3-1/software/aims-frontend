# AIMS Frontend (SHACL Shape Editor)

[[_TOC_]] 

## 📝 Overview

This repository contains the common frontend for the AIMS project. 

The main goal of this frontend application is the editing of [SHACL](https://www.w3.org/TR/shacl/) application/metadata profiles.

If you want to use this frontend against your own shapes repository and vocabulary term service, please provide your own service-provider implementation in [here](./src/util/service-providers/) and edit [this file](./src/util/service-providers/service-provider.ts).

## ⚙️ Configuration

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://git.rwth-aachen.de/coscine/frontend/apps/aimsfrontend)

## 📖 Usage

This project uses [Yarn](https://yarnpkg.com/) and [Node.js](https://nodejs.org/en/) to manage its dependencies.

```
yarn install
```

### Compiles and hot-reloads for development
```
yarn dev
```

### Compiles and minifies for production
```
yarn build
```

### Runs the minified production build
```
yarn preview
```

### Lints files
```
yarn lint
```

### Lints and fixes files
```
yarn lint:fix
```

## 👥 Contributing

As an open source platform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

To contribute to the AIMS Frontend specifically, please have a look at the [AIMS Frontend Contributing Guideline](./CONTRIBUTING.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!


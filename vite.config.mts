/// <reference types="vitest/config" />
import { defineConfig, type UserConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "node:path";

import { nodePolyfills } from "vite-plugin-node-polyfills";
import Components from "unplugin-vue-components/vite";
import AutoImport from "unplugin-auto-import/vite";
import Icons from "unplugin-icons/vite";
import IconsResolver from "unplugin-icons/resolver";
import { BootstrapVueNextResolver } from "bootstrap-vue-next";

// Define the configuration with proper typing
const config: UserConfig = defineConfig({
  resolve: {
    alias: {
      "@": `${path.resolve(__dirname, "src")}`,
    },
    extensions: [".mjs", ".js", ".ts", ".jsx", ".tsx", ".json", ".vue", ".nq"],
  },

  build: {
    commonjsOptions: {
      strictRequires: true,
    },
    rollupOptions: {
      output: {
        manualChunks: {
          "@coscine/api-client": ["@coscine/api-client"],
          "@coscine/form-generator": ["@coscine/form-generator"],
          "@vocabulary/dbo": ["@vocabulary/dbo"],
          "@vocabulary/dc11": ["@vocabulary/dc11"],
          "@vocabulary/dcat": ["@vocabulary/dcat"],
          "@vocabulary/dcterms": ["@vocabulary/dcterms"],
          "@vocabulary/foaf": ["@vocabulary/foaf"],
          "@vocabulary/m4i": ["@vocabulary/m4i"],
          "bootstrap-vue-next": ["bootstrap-vue-next"],
          d3: ["d3"],
          dagre: ["dagre"],
          "rdf-parse": ["rdf-parse"],
        },
      },
    },
  },

  plugins: [
    nodePolyfills(),
    vue(),
    Components({
      dts: "src/components.d.ts",
      include: [/\.vue$/, /\.vue\?vue/],
      exclude: [/[/\\]node_modules[/\\]/, /[/\\]\.git[/\\]/],
      resolvers: [
        BootstrapVueNextResolver(),
        IconsResolver(),
        (componentName) => {
          if (componentName == "FormGenerator") {
            return { name: "default", from: "@coscine/form-generator" };
          }
        },
      ],
    }),
    AutoImport({
      imports: ["vue"],
      dts: "src/auto-imports.d.ts",
      eslintrc: {
        enabled: true, // <-- this
      },
      resolvers: [IconsResolver()],
      dirs: ["./src"],
    }),
    Icons({
      compiler: "vue3",
      autoInstall: true,
      scale: 1,
      defaultClass: "unplugin-icon unplugin-icon-aims",
    }),
  ],
  
  base: "./",

  server: {
    hmr: {
      path: "./",
    },
    host: true,
    port: 9743,
    watch: {
      // Use polling to avoid "too many open files" (EMFILE) errors caused by hitting system file watcher limits.
      // Ignoring node_modules and .git to reduce the load and improve stability.
      usePolling: true,
      interval: 300,
      ignored: ["**/node_modules/**", "**/.git/**"],
    },
  },

  test: {
    globals: true,
    environment: "happy-dom",
    setupFiles: [path.resolve(__dirname, "test/setup.ts")],
    reporters: "dot",
  },
});

export default config;

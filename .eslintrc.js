module.exports = {
  root: true,
  env: {
    node: true,
    es2021: true,
  },
  ignorePatterns: ["node_modules", "build", "coverage", "auto-imports.d.ts", "components.d.ts"],
  plugins: ["eslint-comments", "functional"],
  extends: [
    "eslint:recommended",
    "plugin:unicorn/recommended",
    "plugin:vue/recommended",
    "@vue/eslint-config-typescript/recommended",
    "@vue/eslint-config-prettier",
  ],
  parserOptions: {
    ecmaVersion: "latest",
    "sourceType": "module"
  },
  rules: {
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": "off",
    "vue/v-on-event-hyphenation": "off",
    "unicorn/no-null": "off",
    "unicorn/filename-case": "off",
    "unicorn/prevent-abbreviations": [
      "error",
      {
        "checkFilenames": false
      }
    ],
    "unicorn/no-abusive-eslint-disable": "off",
    "vue/no-v-model-argument": "off"
  }
};
